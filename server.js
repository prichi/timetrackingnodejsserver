var express = require('express')
var bodyParser = require('body-parser');
var AWS = require('aws-sdk');

AWS.config.update({
    region: "eu-central-1",
    endpoint: "https://dynamodb.eu-central-1.amazonaws.com"
});

var docClient = new AWS.DynamoDB.DocumentClient({region:"eu-central-1"});

var port = process.env.PORT || 3000;
var app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended : false}));

app.post('/addRecord', function(req, res) {

    if((!req.body.project || (typeof req.body.project) != "string"
        || !req.body.start || (typeof req.body.start) != "string"
        || !req.body.end || (typeof req.body.end) != "string")) {
        res.status(400).send("400 Bad Request");
    } else {
        var projectName = req.body.project;
        var start = req.body.start;
        var end = req.body.end;

        var params = {
            TableName: "ProjectRecords",
            Item: {
                "ProjectName": projectName,
                "Start": start,
                "End": end
            }
        };

        docClient.put(params, function(err, data) {
            if(err) {
                res.status(200).send("Da hat was beim Schreiben in die DynamoDb ned funktioniert..." + JSON.stringify(err, null, 2));
                console.log("Da hat was beim Schreiben in die DynamoDb ned funktioniert...", JSON.stringify(err, null, 2));
            } else {
                res.status(200).send("Added Item: " + JSON.stringify(data, null, 2));
                console.log("Added Item: ", JSON.stringify(data, null, 2));
            }
        });
        res.status(200).end()
    }
});

app.listen(port);

app.get('/records', function(req, res) {
    var params = {
        TableName: "ProjectRecords",
    };

    docClient.scan(params, function(err, data) {
        if(err) {
            console.error("Unable to read item. Error JSON:", JSON.stringify(err, null, 2));
            res.status(200).send("Unable to read item. Error JSON: " + err);
        } else {
            res.header('Access-Control-Allow-Origin', '*');
            res.json(data);
        }
    });
});

/*
app.get('/sortedRecords/:fieldToSort/:ascDesc', function(req, res) {

    var listOfFields = ['project', 'start', 'end'];
    var listOfAsc = ['1', '-1'];
    if(listOfFields.indexOf(req.params.fieldToSort) == -1
        || listOfAsc.indexOf(req.params.ascDesc) == -1) {

        res.status(400).send("400 Bad Request");
    } else {

        var sortPref = JSON.parse('{"'+ req.params.fieldToSort +'": ' + req.params.ascDesc + '}')
        var sortedProjectRecords = ProjectRecord.find({}).sort(sortPref).exec(function(err, sortedRecords) {
            if(err) {
                return console.error(err);
            }
            res.json({projectRecords: sortedRecords});
        });
    }
})
*/


